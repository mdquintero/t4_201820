package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Calendar;

import api.IDivvyTripsManager;
import model.data_structures.LinkedList;
import model.data_structures.Node;
import model.data_structures.Quick;
import model.data_structures.Shell;
import model.data_structures.Stack;
import model.vo.Station;
import model.vo.VOTrip;

public class DivvyTripsManager implements IDivvyTripsManager {


	private Stack<VOTrip> stackTrips = new Stack<VOTrip>();
	
	private LinkedList<VOTrip> listNTrips = new LinkedList<VOTrip>();

	private LinkedList<Station> listaEncadenadaStations= new LinkedList<Station>();


	public void loadStations (String stationsFile) {
		try{
			FileReader fr = new FileReader(new File(stationsFile));
			BufferedReader br = new BufferedReader(fr);

			String line = br.readLine();
			//Se avanza de nuevo porque la l�nea anterior no contiene datos
			line = br.readLine();

			while(line!=null)
			{


				String[] lineArray = line.split(",");

				Station s = new Station(lineArray[0], lineArray[1], lineArray[2], Double.parseDouble(lineArray[3]), Double.parseDouble(lineArray[4]), 
						Integer.parseInt(lineArray[5]), Calendar.getInstance());


				listaEncadenadaStations.add(s);

				line = br.readLine();
			}

			br.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

	}

	public void loadTrips (String tripsFile) {



		try{
			FileReader fr = new FileReader(new File(tripsFile));
			BufferedReader br = new BufferedReader(fr);

			VOTrip t=null;



			String line = br.readLine();
			//Se avanza de nuevo porque la l�nea anterior no contiene datos
			line = br.readLine();

			while(line!=null)
			{

				String[] lineArray = line.split(",");



				if(lineArray[10].equals("\"\"")){

					t = new VOTrip(Integer.parseInt(lineArray[0].replace("\"","")), lineArray[1].replace("\"",""), lineArray[2].replace("\"",""), Integer.parseInt(lineArray[3].replace("\"","")), 
							Integer.parseInt(lineArray[4].replace("\"","")), Integer.parseInt(lineArray[5].replace("\"","")), lineArray[6], Integer.parseInt(lineArray[7].replace("\"","")), lineArray[8], lineArray[9]);

				}
				else{
					t = new VOTrip(Integer.parseInt((lineArray[0].replace("\"",""))), lineArray[1].replace("\"",""), lineArray[2].replace("\"",""), Integer.parseInt(lineArray[3].replace("\"","")), 
							Integer.parseInt(lineArray[4].replace("\"","")), Integer.parseInt(lineArray[5].replace("\"","")), lineArray[6], Integer.parseInt(lineArray[7].replace("\"","")), lineArray[8], lineArray[9],
							lineArray[10], Integer.parseInt(lineArray[11].replace("\"","")));

				}

				stackTrips.push(t);
				line=br.readLine();

			}

			br.close();

		}
		catch(Exception e){
			e.printStackTrace();
		}


	}

	public void NTrips(int n)
	{
		int sobrante;
		if(n>1119814)
		{
			System.out.println("Por favor ingrese un n�mero en el rango");
			return;
		}else
		{
			sobrante = stackTrips.size()-n;
			for(int i=0; i<n; i++)
			{
				VOTrip actu = stackTrips.pop();
				listNTrips.add(actu);
			}
			System.out.println("Se ha generado una muestra con " + n + " datos. Cantidad sobrante " + sobrante );
		}
		
		
	}
	
	public void quickSorting() {
		long inTime=System.currentTimeMillis();
		VOTrip[] a = new VOTrip[listNTrips.size()];
		Node<VOTrip> item = listNTrips.getFirst();
		for(int i=0; i<listNTrips.size();i++) {
			a[i]=item.getItem();
			item=item.getNext();
		}
		Quick.sort(a);
		System.out.println("Se orden� utilizando Quick Sorting en un tiempo de " + (System.currentTimeMillis()-inTime));
	}
	
	public void shellSorting() {
		
		long inTime=System.currentTimeMillis();
		VOTrip[] a = new VOTrip[listNTrips.size()];
		Node<VOTrip> item = listNTrips.getFirst();
		for(int i=0; i<listNTrips.size();i++) {
			a[i]=item.getItem();
			item=item.getNext();
		}
		Shell.sort(a);
		System.out.println("Se orden� utilizando Shell Sorting en un tiempo de " + (System.currentTimeMillis()-inTime));
	}



}
