package model.vo;

import java.util.Calendar;

public class Station 
{
	//id,name,city,latitude,longitude, dpcapacity,online_date,
	
	//ATRIBUTOS
	
	private String id;
	
	private String name;
	
	private String city;
	
	private double latitude;
	
	private double longitude;
	
	private int dpcapacity;
	
	private Calendar online_date;
	
	
	//CONSTRUCTOR
	
	public Station(String pId, String pName, String pCity, double pLatitude, double pLongitude, int pDpcapacity, Calendar pOnline_date){
		this.setId(pId);
		this.setCity(pCity);
		this.setDpcapacity(pDpcapacity);
		this.setLatitude(pLatitude);
		this.setLongitude(pLongitude);
		this.setName(pName);
		this.setOnline_date(pOnline_date);
	}

	//M�TODOS

	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public double getLatitude() {
		return latitude;
	}


	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}


	public double getLongitude() {
		return longitude;
	}


	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}


	public int getDpcapacity() {
		return dpcapacity;
	}


	public void setDpcapacity(int dpcapacity) {
		this.dpcapacity = dpcapacity;
	}


	public Calendar getOnline_date() {
		return online_date;
	}


	public void setOnline_date(Calendar online_date) {
		this.online_date = online_date;
	}
}
