package model.vo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Representation of a Trip object.
 * La comparación “natural” de trips debe hacerse por su BikeId.
Si el BikeId de los trips es igual, la comparación la define lafecha/hora de inicio de los viajes.
 */
public class VOTrip implements Comparable<VOTrip> {

	//ATRIBUTOS

	SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/YYYY HH:mm");

	private int id;

	private Date strTime;

	private Date endTime;

	private int bikeId;

	private int tripDuration;

	private int idFromStation;

	private String nameFromStation;

	private int idToStation;

	private String nameToStation;

	private String userType;

	private String gender;

	private int birthday;

	//CONSTRUCTORES

	public VOTrip( int pId, String pStrTime, String pEndTime, int pBikeId, int pTripDuration, 
			int pIdFromStation, String pNameFromStation, int pIdToStation, String pNameToStation,
			String pUserType, String pGender, int pBirthday) throws Exception {


		this.setId(pId);
		this.setStrTime(dateFormat.parse(pStrTime));
		this.setEndTime(dateFormat.parse(pEndTime));
		this.setBikeId(pBikeId);
		this.setTripDuration(pTripDuration);
		this.setIdFromStation(pIdFromStation);
		this.setNameFromStation(pNameFromStation);
		this.setIdToStation(pIdToStation);
		this.setNameToStation(pNameToStation);
		this.setUserType(pUserType);
		this.setGender(pGender);
		this.setBirthday(pBirthday);
	}

	public VOTrip( int pId, String pStrTime, String pEndTime, int pBikeId, int pTripDuration, 
			int pIdFromStation, String pNameFromStation, int pIdToStation, String pNameToStation,
			String pUserType) throws Exception {

		this.setId(pId);
		this.setStrTime(dateFormat.parse(pStrTime));
		this.setEndTime(dateFormat.parse(pEndTime));
		this.setBikeId(pBikeId);
		this.setTripDuration(pTripDuration);
		this.setIdFromStation(pIdFromStation);
		this.setNameFromStation(pNameFromStation);
		this.setIdToStation(pIdToStation);
		this.setNameToStation(pNameToStation);
		this.setUserType(pUserType);

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getStrTime() {
		return strTime;
	}

	public void setStrTime(Date strTime) {
		this.strTime = strTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public int getBikeId() {
		return bikeId;
	}

	public void setBikeId(int bikeId) {
		this.bikeId = bikeId;
	}

	public int getTripDuration() {
		return tripDuration;
	}

	public void setTripDuration(int tripDuration) {
		this.tripDuration = tripDuration;
	}

	public int getIdFromStation() {
		return idFromStation;
	}

	public void setIdFromStation(int idFromStation) {
		this.idFromStation = idFromStation;
	}

	public String getNameFromStation() {
		return nameFromStation;
	}

	public void setNameFromStation(String nameFromStation) {
		this.nameFromStation = nameFromStation;
	}

	public int getIdToStation() {
		return idToStation;
	}

	public void setIdToStation(int idToStation) {
		this.idToStation = idToStation;
	}

	public String getNameToStation() {
		return nameToStation;
	}

	public void setNameToStation(String nameToStation) {
		this.nameToStation = nameToStation;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getBirthday() {
		return birthday;
	}

	public void setBirthday(int birthday) {
		this.birthday = birthday;
	}
	/**
	 * @return id - Trip_id
	 */
	public int id() {
		return getId();
	}	


	/**
	 * @return time - Time of the trip in seconds.
	 */
	public double getTripSeconds() {
		return getTripDuration();
	}

	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStation() {

		return getNameFromStation();
	}

	/**
	 * @return station_name - Destination Station Name .
	 */
	public String getToStation() {

		return getNameToStation();
	}

	@Override
	public int compareTo(VOTrip o) {
		
		if(this.bikeId<o.bikeId)
			return -1;
		else if(this.bikeId>o.bikeId)
			return 1;
		else
		{
			if(strTime.before(endTime))
				return -1;
			else
				return 1;
		}
	}
}
