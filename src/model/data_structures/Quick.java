package model.data_structures;

import java.util.Random;


public class Quick<T> {


	//SORTING

	public static <T> void sort(Comparable<T>[] a) {
		shuffleArray(a);
		sort(a, 0, a.length - 1);
	}


	public static <T> void sort(Comparable<T>[] a, int low, int high) {
		if (high <= low) return;
		int j = partition(a, low, high);
		sort(a, low, j-1);
		sort(a, j+1, high);
	}


	public static <T> int partition(Comparable<T>[] a, int low, int high) {
		int i = low;
		int j = high;
		Comparable<T> b= a[low];

		while(true) {

			//encontrar 
			while(less(a[++i], b))
				if(i==high) {break;}

			while(less(a[--j], b))
				if(j==low) {break;}

			if(i>=j) break;

			exchange(a, i, j);
		}

		exchange (a, low, j);

		return j;

	}


	//METODOS ADICIONALES

	//v<w
	private static boolean less(Comparable v, Comparable w) {
		if (v == w) return false;   // optimization when reference equals
		return v.compareTo(w) < 0;
	}

	//swap i with j
	private static void exchange(Object[] a, int i, int j) {
		Object swap = a[i];
		a[i] = a[j];
		a[j] = swap;
	}

	//shuffles an array
	public static <T> void shuffleArray(Comparable<T>[] ar)
	{

		Random rnd = new Random();
		for (int i = ar.length - 1; i > 0; i--)
		{
			int index = rnd.nextInt(i + 1);
			// Simple swap
			Comparable<T> a = ar[index];
			ar[index] = ar[i];
			ar[i] = a;
		}
	}
}
