package model.data_structures;

public class Shell {



	private static boolean less(Comparable v , Comparable w) 
	{ 
		return v.compareTo(w) < 0; 
	}
	private static <T> void exch (Comparable<T>[] a,int i,int j) 
	{ 
		Comparable<T> t= a[i]; 
		a[i] = a[j]; 
		a[j] = t; 
	}


	public static<T> void sort(Comparable<T>[] a)
	{ 
		int N= a.length;int h= 1;
		while(h< N/3) h= 3*h+ 1; 
		while
			(h>= 1) 
		{ 
			for(int i= h;i< N; i++) 
			{ 
				for(int j= i; j>= h && less(a[j], a[j-h]); j-= h)
					exch (a, j, j-h);
			}
			h = h/3;
		}
	}
}
