package api;


import model.data_structures.LinkedList;
import model.vo.VOTrip;

/**
 * Basic API for testing the functionality of the STS manager.
 */
public interface IDivvyTripsManager {

	/**
	 * Method to load the Divvy trips of the STS
	 * @param tripsFile - path to the file 
	 */
	void loadTrips(String tripsFile);
	
	
	/**
	 * Method to load the Divvy Stations of the STS
	 * @param stationsFile - path to the file 
	 */
	void loadStations(String stationsFile);
	
	void NTrips(int n);
	
	void quickSorting();

	void shellSorting();
}
