package view;

import java.util.Scanner;

import controller.Controller;

import model.data_structures.LinkedList;
import model.vo.VOTrip;

public class DivvyTripsManagerView 
{
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			printMenu();
			
			int option = sc.nextInt();
			
			switch(option)
			{
				case 1:
					Controller.loadTrips();
					break;
					
				case 2:
					System.out.println("Ingrese el tama�o de la muestra a generar");
					int tamano = sc.nextInt();
					Controller.nTrips(tamano);
					break;
					
				case 3:
					Controller.quickSorting();
					break;
					
				case 4:
					Controller.shellSorting();
					break;
					
				case 5:	
					fin=true;
					sc.close();
					break;
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 4----------------------");
		System.out.println("1. Cargar los datos de viajes registrados en el archivo Divvy_Trips_2017_Q2");
		System.out.println("2. Generar una muestra de N viajes. El valor m�ximo de N es 1.119.814");
		System.out.println("3. Ordenar la muestra generada utilizando Quick Sorting");
		System.out.println("4. Ordenar la muestra generada utlizando Shell Sorting");
		System.out.println("5. Salir");
		System.out.println("Digite el n�mero de opci�n para ejecutar la tarea, luego presione enter: (Ej., 1):");
		
	}
}
