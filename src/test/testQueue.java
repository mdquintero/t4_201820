package test;

import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.Queue;

public class testQueue {


	//ATRIBUTOS
	private Queue<Integer> queueIntegers;


	private Queue<String> queueStrings;


	//METODOS
	private void setupEscenarioIntegers(){

		queueIntegers = new Queue<Integer>();

		queueIntegers.enqueue(new Integer(1));
		queueIntegers.enqueue(new Integer(2));
		queueIntegers.enqueue(new Integer(3));
		queueIntegers.enqueue(new Integer(4));
		queueIntegers.enqueue(new Integer(5));
		queueIntegers.enqueue(new Integer(6));
		queueIntegers.enqueue(new Integer(7));

	}

	private void setupEscenarioStrings(){

		queueStrings = new Queue<String>();

		queueStrings.enqueue("aaa");
		queueStrings.enqueue("bbb");
		queueStrings.enqueue("ccc");
		queueStrings.enqueue("ddd");
		queueStrings.enqueue("fff");
		queueStrings.enqueue("ggg");
		queueStrings.enqueue("hhh");

	}

	
	@Test
	public void testEnqueue(){
		setupEscenarioIntegers();
		setupEscenarioStrings();


		assertEquals( "El metodo enqueue no agrego todos los elementos a la lista", 7, queueIntegers.size() );
		assertEquals( "El metodo enqueue no agrego todos los elementos a la lista", 7, queueStrings.size() );

	}

	

	@Test
	public void testDequeue(){
		setupEscenarioIntegers();
		setupEscenarioStrings();


		queueStrings.dequeue();
		queueIntegers.dequeue();

		assertEquals( "El metodo dequeue no elimino el elemento de la lista", 6, queueIntegers.size() );
		assertEquals( "El metodo dequeue no elimino el elemento de la lista", 6, queueStrings.size() );
	}

	@Test
	public void testIsEmpty(){
		queueIntegers = new Queue<Integer>();
		queueStrings = new Queue<String>();

		assertTrue( "La lista deberia estar vacia", queueIntegers.isEmpty() );
		assertTrue( "La lista deberia estar vacia", queueStrings.isEmpty() );
	}

	
	
	
}
