package test;
import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.LinkedList;
import model.data_structures.Node;
import model.data_structures.Quick;
import model.data_structures.Shell;
import model.vo.VOTrip;


public class testSorting {

	private LinkedList<Integer> lista;
	
	
	public void setup1()
	{
		lista = new LinkedList<Integer>();
		lista.add(1);
		lista.add(2);
		lista.add(3);
		lista.add(4);
		lista.add(5);
		lista.add(6);
		lista.add(7);
		lista.add(8);
	}
	
	public void setup2()
	{
		lista = new LinkedList<Integer>();
		lista.add(7);
		lista.add(3);
		lista.add(8);
		lista.add(5);
		lista.add(2);
		lista.add(6);
		lista.add(4);
		lista.add(1);
	}
	
	public void setup3()
	{
		lista = new LinkedList<Integer>();
		lista.add(4);
		lista.add(2);
		lista.add(1);
		lista.add(4);
		lista.add(5);
		lista.add(3);
		lista.add(5);
		lista.add(6);
		
	}
	
	
	@Test
	public void shellSort()
	{
		setup1();
			Integer[] a = new Integer[lista.size()];
			Node<Integer> item = lista.getFirst();
			for(int i=0; i<lista.size();i++) {
				a[i]=item.getItem();
				item=item.getNext();
			}
			Shell.sort(a);
			assertTrue("El ordenamiento no funcion�", lista.get(0).getItem()==1);
			assertTrue("El ordenamiento no funcion�", lista.get(1).getItem()==2);
			assertTrue("El ordenamiento no funcion�", lista.get(2).getItem()==3);
			assertTrue("El ordenamiento no funcion�", lista.get(3).getItem()==4);
			assertTrue("El ordenamiento no funcion�", lista.get(4).getItem()==5);
			assertTrue("El ordenamiento no funcion�", lista.get(5).getItem()==6);
			assertTrue("El ordenamiento no funcion�", lista.get(6).getItem()==7);
			assertTrue("El ordenamiento no funcion�", lista.get(7).getItem()==8);
			
			setup2();
			Integer[] a2 = new Integer[lista.size()];
			Node<Integer> item2 = lista.getFirst();
			for(int i=0; i<lista.size();i++) {
				a[i]=item.getItem();
				item=item.getNext();
			}
			Shell.sort(a);
			assertTrue("El ordenamiento no funcion�", lista.get(0).getItem()==1);
			assertTrue("El ordenamiento no funcion�", lista.get(1).getItem()==2);
			assertTrue("El ordenamiento no funcion�", lista.get(2).getItem()==3);
			assertTrue("El ordenamiento no funcion�", lista.get(3).getItem()==4);
			assertTrue("El ordenamiento no funcion�", lista.get(4).getItem()==5);
			assertTrue("El ordenamiento no funcion�", lista.get(5).getItem()==6);
			assertTrue("El ordenamiento no funcion�", lista.get(6).getItem()==7);
			assertTrue("El ordenamiento no funcion�", lista.get(7).getItem()==8);
			
			
			setup3();
			Integer[] a3 = new Integer[lista.size()];
			Node<Integer> item3 = lista.getFirst();
			for(int i=0; i<lista.size();i++) {
				a[i]=item.getItem();
				item=item.getNext();
			}
			Shell.sort(a);
			assertTrue("El ordenamiento no funcion�", lista.get(0).getItem()==1);
			assertTrue("El ordenamiento no funcion�", lista.get(1).getItem()==2);
			assertTrue("El ordenamiento no funcion�", lista.get(2).getItem()==3);
			assertTrue("El ordenamiento no funcion�", lista.get(3).getItem()==4);
			assertTrue("El ordenamiento no funcion�", lista.get(4).getItem()==4);
			assertTrue("El ordenamiento no funcion�", lista.get(5).getItem()==5);
			assertTrue("El ordenamiento no funcion�", lista.get(6).getItem()==5);
			assertTrue("El ordenamiento no funcion�", lista.get(7).getItem()==6);
	}
	
	@Test
	public void quickSort()
	{
		setup1();
			Integer[] a = new Integer[lista.size()];
			Node<Integer> item = lista.getFirst();
			for(int i=0; i<lista.size();i++) {
				a[i]=item.getItem();
				item=item.getNext();
			}
			Quick.sort(a);
			assertTrue("El ordenamiento no funcion�", lista.get(0).getItem()==1);
			assertTrue("El ordenamiento no funcion�", lista.get(1).getItem()==2);
			assertTrue("El ordenamiento no funcion�", lista.get(2).getItem()==3);
			assertTrue("El ordenamiento no funcion�", lista.get(3).getItem()==4);
			assertTrue("El ordenamiento no funcion�", lista.get(4).getItem()==5);
			assertTrue("El ordenamiento no funcion�", lista.get(5).getItem()==6);
			assertTrue("El ordenamiento no funcion�", lista.get(6).getItem()==7);
			assertTrue("El ordenamiento no funcion�", lista.get(7).getItem()==8);
			
			setup2();
			Integer[] a2 = new Integer[lista.size()];
			Node<Integer> item2 = lista.getFirst();
			for(int i=0; i<lista.size();i++) {
				a[i]=item.getItem();
				item=item.getNext();
			}
			Quick.sort(a);
			assertTrue("El ordenamiento no funcion�", lista.get(0).getItem()==1);
			assertTrue("El ordenamiento no funcion�", lista.get(1).getItem()==2);
			assertTrue("El ordenamiento no funcion�", lista.get(2).getItem()==3);
			assertTrue("El ordenamiento no funcion�", lista.get(3).getItem()==4);
			assertTrue("El ordenamiento no funcion�", lista.get(4).getItem()==5);
			assertTrue("El ordenamiento no funcion�", lista.get(5).getItem()==6);
			assertTrue("El ordenamiento no funcion�", lista.get(6).getItem()==7);
			assertTrue("El ordenamiento no funcion�", lista.get(7).getItem()==8);
			
			
			setup3();
			Integer[] a3 = new Integer[lista.size()];
			Node<Integer> item3 = lista.getFirst();
			for(int i=0; i<lista.size();i++) {
				a[i]=item.getItem();
				item=item.getNext();
			}
			Quick.sort(a);
			assertTrue("El ordenamiento no funcion�", lista.get(0).getItem()==1);
			assertTrue("El ordenamiento no funcion�", lista.get(1).getItem()==2);
			assertTrue("El ordenamiento no funcion�", lista.get(2).getItem()==3);
			assertTrue("El ordenamiento no funcion�", lista.get(3).getItem()==4);
			assertTrue("El ordenamiento no funcion�", lista.get(4).getItem()==4);
			assertTrue("El ordenamiento no funcion�", lista.get(5).getItem()==5);
			assertTrue("El ordenamiento no funcion�", lista.get(6).getItem()==5);
			assertTrue("El ordenamiento no funcion�", lista.get(7).getItem()==6);
	}
}
